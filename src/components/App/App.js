import React from 'react';
import logo from '../../logo.svg';
import './App.css';
import Button from "@material-ui/core/Button";
import {gql} from "apollo-boost";
import {Query} from "react-apollo";

const FETCH_USERS_QUERY = gql`
    query {
        users {
            id
            email
            name
        }
    }
`;

function App(...props) {
  return (
    <div className="App">
        <Button variant="contained" color="primary">
            Hello World
        </Button>

        <div>
            <Query query={FETCH_USERS_QUERY}>
                {({loading, error, data}) => {
                    if (loading) {
                        console.log("loading");
                        return "loading"
                    }

                    if (error) {
                        return error.message
                    }

                    console.log(data);
                    return "ok"
                }}
            </Query>
        </div>
    </div>
  );
}

export default App;
